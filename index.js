
//////////////////////////////// Bank Section ///////////////////////////////////////
let Balance = 500;
let TotalLoan = 0;


/**
 * Writes balance to html
 */
const WriteBalance = () => {
    const ref = document.getElementById("balance");
    ref.innerText = Balance + " Kr"
};



/**
 * Writes total loan to html
 */
const WriteTotalLoan = () => {
    const loankey = document.getElementById("loankey")
    const loanval = document.getElementById("loanval")

    //If there is no loan, return
    if(TotalLoan < 1 ){
        //Remove loan section from bank card
        loankey.innerText = ""
        loanval.innerText = ""
    } else{
        loankey.innerText = "Loan"
        loanval.innerText = TotalLoan + " Kr"
    }
};


/**
 * Creates a new loan for the user
 */
const MakeLoan = () => {
    const amount = PromptUserLoan()

    if(TotalLoan > 0){
        alert("You can only have one loan at all times. Pay this back to make a new loan request.")
        return
    }
    if(amount > Balance*2){
        alert("You cannot loan more then twice you balance.")
        return
    }

    TotalLoan = isNaN(parseInt(amount)) ? 0 : parseInt(amount);
    WriteTotalLoan()
    RepayBtn()
}



/**
 * Prompts a user for the amount 
 * @returns loan amount
 */
const PromptUserLoan = () => {
    //Prompt user for loan value
    const loanAmount = window.prompt("How much do you intend to loan?")
    
    if(loanAmount == null) return;
    if(isNaN(loanAmount)){
        alert("Input must be a number")
        return
    }

    return loanAmount;    
}


////////////////////////////////// Work section ////////////////////////////////

let Pay = 0;

/**
 * Writes pay to dom
 */
const WritePay = () => {
    const ref = document.getElementById("payval")
    ref.innerText = Pay + " Kr"
}

/**
 * Increases pay by 100 and writes to html
 */
const Work = () => {
    Pay += 100
    WritePay()
}


/**
 * Transfer pay to bank
 */
const Bank = () => {
    if(Pay < 1) {
        alert("Your pay needs to be more then 0 to transfer to bank.")
        return
    }

    //Transfer to bank
    if(TotalLoan){
        TotalLoan -= (Pay * 10) / 100      //deduct from loan
        Balance += Pay - ((Pay * 10) / 100) //Add pay - 10% to balance    
    } else {
        Balance += Pay //Add to pay
    }

    //Reset pay
    Pay = 0 

    //Write to dom
    WriteBalance()
    WriteTotalLoan()
    WritePay()
    RepayBtn()

}



/**
 * Toggles repay loan button
 */
const RepayBtn = () => {
    const btn = document.getElementById("repaybtn")
    if(TotalLoan > 0)
        btn.style.visibility = "visible"
    else
        btn.style.visibility = "hidden"
}



/**
 * Deduct entire pay from loan
 */
const Repay = () => {
    TotalLoan -= Pay
    Pay = 0

    //If pay was more than total loan - add remaining pay to balance
    if(TotalLoan < 0){
        Balance += Math.abs(TotalLoan)
    }

    WriteBalance();
    WriteTotalLoan();
    WritePay();
    RepayBtn();
}



/////////////////////// Laptops section ///////////////////////

let laptops;
let selected = 0;

/**
 * Makes a get request to api and creates list items in ul
 * @returns List of computers
 */
const Computers = async () => {
    const res = await fetch("https://hickory-quilled-actress.glitch.me/computers")
    laptops = await res.json()

    //Set dropdown starting item
    DropDownTitle(0)
    ComputerFeatures(0)
    ComputerInformation(0)
    
    
    //write laptops to dropdown
    const ref = document.getElementById("dropdown")
    for (let entry in laptops){
        var li = document.createElement('li')
        li.classList.add("dropdown-item")
        li.value = entry
        li.onclick = (e) =>{
            selected = e.target.value
            DropDownTitle(selected) 
            ComputerFeatures(selected)
            ComputerInformation(selected)
        }
        li.innerText = laptops[entry].title
        ref.appendChild(li)
    }

}

/**
 * Changes title of dropdown
 * @param {*} val - index of computer
 */
const DropDownTitle = (val) => {
    const ref = document.getElementById("dropdownbtn")
    ref.innerText = laptops[val].title
}


/**
 * Writes features of a computer to dom 
 * @param {*} val - index of computer
 */
const ComputerFeatures = (val) => {
    const ref = document.getElementById("laptopbody")
    
    
    //Remove previous information 
    var elem1 = document.getElementById("title");
    var elem2 = document.getElementById("list");
    if(elem1 != null && elem2 != null){
        elem1.parentNode.removeChild(elem1)
        elem2.parentNode.removeChild(elem2)
    }


    //Title
    var specstitle = document.createElement('p')
    specstitle.classList.add("card-text")
    specstitle.innerText = "Features:"
    specstitle.style.fontWeight = "bold"
    specstitle.style.marginTop = "15px";
    specstitle.id = "title"
    ref.appendChild(specstitle)
    
    //List
    var ul = document.createElement('ul')
    ul.id = "list"
    for(let i = 0; i < laptops[val].specs.length; i++){    
        var li = document.createElement('li')    
        li.innerText += laptops[val].specs[i]
        ul.appendChild(li)
    }
    ref.appendChild(ul)
        
        
}

/**
 * Writes information about the computer to the dom
 */
const ComputerInformation = async (index) => {
    const curr = laptops[index]
    console.log(curr)

    //Set title
    document.getElementById("infotitle").innerText = curr.title

    //Set image
    document.getElementById("computerimg").src = "https://hickory-quilled-actress.glitch.me/" + curr.image

    //Set description
    document.getElementById("computerdescripton").innerText = curr.description

    //Set price
    document.getElementById("price").innerText = "Price: " + curr.price + " Kr"
}


/**
 * Buys a computer
 */
const BuyNow = () => {
    if(Balance < laptops[selected].price){
        alert("You do not have money for this computer. Work a bit more.")
        return
    }

    Balance -= laptops[selected].price
    WriteBalance()
    alert("Congratulations you just bought the computer \"" + laptops[selected].title + "\". You will recieve it shortly.")
}




////////////////////// Startup /////////////////////////

WriteBalance();
WriteTotalLoan();
RepayBtn();
Computers();