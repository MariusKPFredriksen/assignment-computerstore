# Assignment-ComputerStore
I this project I have created a dynamic webpage using html, js and bootstrap. The webpage allows for a user to take a see their bank balance, take a loan, pay back this loan and buy a computer selected from a dropdown button.

## To run 
* Open in VSCode
* Install live server plugin
* Go live 

## Contributers
* Marius Fredriksen (marfre52494@candidate.noroff.no)

